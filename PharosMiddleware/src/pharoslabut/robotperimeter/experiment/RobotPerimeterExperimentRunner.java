package pharoslabut.robotperimeter.experiment;

import pharoslabut.navigate.Location;
import pharoslabut.navigate.MotionArbiter;
import pharoslabut.robotperimeter.Mobility;
import pharoslabut.robotperimeter.Vision;
import pharoslabut.sensors.CompassDataBuffer;
import pharoslabut.sensors.GPSDataBuffer;

public class RobotPerimeterExperimentRunner extends Thread {
	private static int numTargets;

	private double fakeLatitude;
	private double fakeLongitude;

	private boolean targetTracker;
	private boolean staticTarget;
	private boolean goToGoal;

	private CompassDataBuffer compassDataBuffer;
	private GPSDataBuffer gpsDataBuffer;
	private MotionArbiter motionArbiter;
	private double forceField;
	private double finalPositionTolerance;
	private boolean isTargetSource;
	private boolean expTargetSource;
	private int targetNumber;

	public RobotPerimeterExperimentRunner(int numTargets, double fakeLatitude,
			double fakeLongitude, boolean targetTracker, boolean staticTarget,
			CompassDataBuffer compassDataBuffer, GPSDataBuffer gpsDataBuffer,
			MotionArbiter motionArbiter, double forceField, boolean goToGoal,
			double finalPositionTolerance, int targetNumber,
			boolean isTargetSource, boolean expTargetSource) {
		this.fakeLatitude = fakeLatitude; // Latitude for either robot's goal or
											// target's initial location
		this.fakeLongitude = fakeLongitude;
		this.targetTracker = targetTracker; // robot goals to target's location
		this.staticTarget = staticTarget; // target does not move
		this.goToGoal = goToGoal; // robot goes to predefined goal, does not
									// share this goal with others

		this.compassDataBuffer = compassDataBuffer;
		this.gpsDataBuffer = gpsDataBuffer;
		this.motionArbiter = motionArbiter;
		this.forceField = forceField;
		this.finalPositionTolerance = finalPositionTolerance;

		this.targetNumber = targetNumber;
		this.isTargetSource = isTargetSource;
		this.expTargetSource = expTargetSource;
	}

	@Override
	public void run() {

		Mobility mobility = new Mobility(motionArbiter, compassDataBuffer,
				gpsDataBuffer, numTargets, targetTracker, forceField, goToGoal,
				fakeLatitude, fakeLongitude, finalPositionTolerance,
				isTargetSource, targetNumber, staticTarget/*
														 * ,
														 * wifibeaconbroadcaster
														 */);
		MobilityThread mobilityThread = new MobilityThread(mobility);
		mobilityThread.start();

		if (!isTargetSource) {
			Vision vision = new Vision(fakeLatitude, fakeLongitude,
					targetTracker, staticTarget, goToGoal, expTargetSource);
			VisionThread visionThread = new VisionThread(vision);
			visionThread.start();
		}

		while (true) {
			try {
				Thread.sleep(Long.MAX_VALUE);
			} catch (InterruptedException e) { /* Ignore */
			}
		}
	}

	class MobilityThread extends Thread {
		Mobility mobility;

		public MobilityThread(Mobility mobility) {
			this.mobility = mobility;
		}

		public void run() {
			mobility.controlMotionAndIntelligence();
		}
	}

	class VisionThread extends Thread {
		Vision vision;

		public VisionThread(Vision vision) {
			this.vision = vision;
		}

		public void run() {
			vision.VisionControl();
		}
	}
}