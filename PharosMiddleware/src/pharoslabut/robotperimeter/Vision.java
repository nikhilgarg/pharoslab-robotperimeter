package pharoslabut.robotperimeter;

import pharoslabut.logger.Logger;
import pharoslabut.navigate.Location;
import edu.utexas.ece.mpc.context.ContextHandler;
import edu.utexas.ece.mpc.context.group.GroupDefinition;
import edu.utexas.ece.mpc.context.summary.ContextSummary;
import edu.utexas.ece.mpc.context.summary.GroupContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapGroupContextSummary;
import edu.utexas.ece.mpc.context.util.GroupUtils;

public class Vision {

   private static ContextHandler handler;
   static double fakeLatitude;
   static double fakeLongitude;
   static boolean targetTracker;
   static boolean staticTarget;
   static boolean goToGoal;
   static boolean expTargetSource;

    public Vision(double fLat, double fLong, boolean targetTracker, boolean staticTarget, boolean goToGoal, boolean expTargetSource) {
        Logger.log("created vision");
        handler = ContextHandler.getInstance();
        fakeLatitude = fLat;
        fakeLongitude = fLong;
        Vision.targetTracker = targetTracker;
        Vision.staticTarget = staticTarget;
        Vision.goToGoal = goToGoal;
        Vision.expTargetSource = expTargetSource;
    }

    public void VisionControl() {
        Logger.log("Started vision control");
        // for now, faking target messages (from other robots)
        // later, will receive messages from target with gps location
        if (goToGoal) return; //vision not needed to just go to goal
        if (expTargetSource) return; // fake vision not needed if the experiment has a targetSource
        
        receivedTargetSignal(new Location(fakeLatitude, fakeLongitude), 0);
        long sysTime = System.currentTimeMillis();
        Logger.log("Sending faked lat/long");
        if (targetTracker)
            return; // don't want to fake sense a target if just following target
        while (!staticTarget) {

            if (System.currentTimeMillis() - sysTime > 2 * 1000) {
                // fakeLatitude += /*Math.random()*.001*/ - Math.random()*.001;
                fakeLongitude += .00002; // Math.random()*.001 - Math.random()*.001;

                // fakeLatitude += .001;
                // fakeLongitude += .001;

                Logger.log(String.format("Sending new fake location: (%f, %f)", fakeLatitude,
                                         fakeLongitude));
                receivedTargetSignal(new Location(fakeLatitude, fakeLongitude), 0);
                sysTime = System.currentTimeMillis();
                Logger.log("ended loop in VisionControl...starting over");

            }

        }
    }

    public static void receivedTargetSignal(Location targetLocation, int targetId) {

        Logger.log("Received target signal");
        findTarget(targetLocation, targetId);
    }

    public static void findTarget(Location location, int targetId) {

        HashMapContextSummary mySummary = Intelligence.mySummary;
        TargetSighting newSighting = new TargetSighting();
        newSighting.targetId = targetId;
        newSighting.hostId = Intelligence.myId;

        newSighting.timestamp = System.currentTimeMillis();
        newSighting.targetLocation = new LocationStamp(location, newSighting.timestamp);

        Logger.log("Inserting target sighting into local summary");
        HashMapContextSummaryInterface.insertTargetSighting(mySummary, newSighting);

        Logger.log("Updating summary with ContextHandler");
        handler.updateLocalSummary(mySummary);

        // create target group (or add local summary if already exists)

        ContextSummary myTargetGroup  = handler.getGroupSummary(targetId);

        if (myTargetGroup == null) {
            GroupUtils.addDeclaredGroupMembership(mySummary, targetId);
            Logger.log("Declared group members after adding: "
                       + GroupUtils.getDeclaredMemberships(mySummary));
            handler.updateLocalSummary(mySummary);
            Logger.log("Updated Local Summary:" + mySummary);
            GroupDefinition groupDef = new TargetGroupDefinition(targetId);
            handler.addGroupDefinition(groupDef);
            Logger.log("added group Definition");
            handler.updateLocalSummary(mySummary);

            Logger.log("myTargetGroup: " + handler.getGroupSummary(targetId).toString());
            Logger.log("mySummary: " + mySummary.toString());
        } else {
            handler.updateLocalSummary(mySummary);
        }
    }

}
